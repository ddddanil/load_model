mod config;
// mod unbuffered_channel;
mod error_handler;
mod generator;
mod message_buffer;
mod output_balancer;
mod sink;

use std::{fmt, fs::{self, File}, iter, path::Path, sync::Arc};

use color_eyre::Report;
use futures::future;
use tokio::task::JoinHandle;
use tracing::{debug_span, error, instrument, info_span};
use tracing_forest::Processor;
use tracing_subscriber::util::SubscriberInitExt;

#[instrument]
async fn construct_system(
    config: &config::Config,
    task_span: &tracing::Span,
) -> Vec<JoinHandle<()>> {
    let (generators, gen_receivers): (Vec<_>, Vec<_>) = config
        .generators
        .iter()
        .enumerate()
        .map(|(i, gen_config)| {
            generator::SteadyGenerator::with_channel(
                gen_config.period,
                message::generator_message(i),
                &task_span,
            )
        })
        .unzip();
    let (sinks, sink_senders): (Vec<_>, Vec<_>) = config
        .sinks
        .iter()
        .map(|sink_config| {
            sink::ExponentialSink::with_channel(
                sink_config.initial_delay,
                sink_config.delay_coeff,
                &task_span,
            )
        })
        .unzip();
    let output_balancer =
        output_balancer::OutputBalancer::new(sink_senders.into_boxed_slice(), &task_span);
    let (error_handler, error_channel) = error_handler::ErrorHandler::with_channel(&task_span);
    let message_buffer = Arc::new(message_buffer::MessageBuffer::new(
        config.buffer_capacity,
        gen_receivers.into_boxed_slice(),
        error_channel,
        output_balancer,
        &task_span,
    ));
    generators
        .into_iter()
        .map(|g| tokio::spawn(g.run()))
        .chain(sinks.into_iter().map(|s| tokio::spawn(s.run())))
        .chain(iter::once(error_handler).map(|h| tokio::spawn(h.run())))
        .chain(iter::once(message_buffer).flat_map(|b| {
            let input = tokio::spawn(b.clone().run_input());
            let output = tokio::spawn(b.clone().run_output());
            [input, output]
        }))
        .collect()
}

#[instrument]
fn read_config<P>(path: P) -> Result<config::Config, Report>
where
    P: AsRef<Path> + fmt::Debug,
{
    let contents = fs::read(path)?;
    let config = toml::from_slice(&contents)?;
    Ok(config)
}

#[instrument]
fn tracing_jaeger_subscriber() -> Result<impl tracing::Subscriber, Report> {
    use tracing_error::ErrorLayer;
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::EnvFilter;
    use tracing_tree::HierarchicalLayer;
    // global::set_text_map_propagator(opentelemetry_jaeger::Propagator::new());
    let tracer = opentelemetry_jaeger::new_agent_pipeline()
        .with_service_name("load_model")
        .install_batch(opentelemetry::runtime::Tokio)?;
    let telemetry_layer = tracing_opentelemetry::layer().with_tracer(tracer);
    let subscriber = tracing_subscriber::Registry::default()
        .with(EnvFilter::from_default_env())
        .with(
            HierarchicalLayer::new(2)
                .with_targets(true)
                .with_bracketed_fields(true),
        )
        .with(telemetry_layer)
        .with(ErrorLayer::default());
        // .init();
    Ok(subscriber)
}

#[instrument]
fn tracing_fmt_subscriber() -> Result<impl tracing::Subscriber, Report> {
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::filter::filter_fn;
    let fmt_layer = tracing_subscriber::fmt::layer();

    let subscriber = tracing_subscriber::registry()
        .with(fmt_layer.with_filter(filter_fn(|metadata| {
            metadata.target() == "lifecycle_events"
        })));
    Ok(subscriber)
}

#[instrument]
fn setup() -> Result<(), Report> {
    if std::env::var("RUST_LIB_BACKTRACE").is_err() {
        std::env::set_var("RUST_LIB_BACKTRACE", "1")
    }
    color_eyre::install()?;

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info")
    }
    Ok(())
}

#[instrument]
async fn run_task(config: &config::Config) {
    let task_span = info_span!(target: "services", "Task");
    let tasks = construct_system(config, &task_span).await;
    for error in future::join_all(tasks)
        .await
        .iter()
        .filter_map(|r| r.as_ref().err())
    {
        error!("{error:?}");
    }
}

#[tokio::main]
async fn main() -> Result<(), Report> {
    setup()?;
    tracing_fmt_subscriber()?.init();

    let config = read_config("task.toml")?;
    run_task(&config).await;
    // let out = File::create("out.log")?;

    // tracing_forest::worker_task()
        // .map_receiver(|printer| printer
        //               .writer(out)
        //               .or_stderr()
        // )
        // .build()
        // .on(run_task(&config))
        // .await;
    Ok(())
}
