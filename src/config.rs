use std::time;

use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct GeneratorConfig {
    #[serde(with="humantime_serde")]
    pub period: time::Duration,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct SinkConfig {
    #[serde(with="humantime_serde")]
    pub initial_delay: time::Duration,
    pub delay_coeff: f64,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Config {
    pub generators: Vec<GeneratorConfig>,
    pub sinks: Vec<SinkConfig>,
    pub buffer_capacity: usize,
}
