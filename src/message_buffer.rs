use futures::future;
use std::{fmt::Debug, mem, ops::DerefMut, sync::Arc, time};
use tokio::sync::{
    mpsc::{Receiver, Sender},
    Mutex,
};
use tracing::{debug_span, info, info_span, instrument, Instrument};

use crate::output_balancer::OutputBalancer;

#[derive(Debug, Clone)]
pub struct MessageEntry<T> {
    registered: time::Instant,
    source_id: usize,
    payload: T,
    tracing_span: tracing::Span,
}

#[derive(Debug)]
pub struct MessageBuffer<T> {
    message_buffer: Mutex<(usize, Box<[Option<MessageEntry<T>>]>)>,
    input_channels: Mutex<Box<[Receiver<T>]>>,
    error_channel: Sender<MessageEntry<T>>,
    output_balancer: Mutex<OutputBalancer<T>>,
    message_buffer_span: tracing::Span,
}

impl<T> MessageBuffer<T> {
    #[instrument]
    pub fn new(
        capacity: usize,
        input_channels: Box<[Receiver<T>]>,
        error_channel: Sender<MessageEntry<T>>,
        output_balancer: OutputBalancer<T>,
        parent_span: &tracing::Span,
    ) -> Self {
        let buffer = {
            let mut buffer = Vec::new();
            buffer.resize_with(capacity, Default::default);
            buffer.into_boxed_slice()
        };
        MessageBuffer {
            message_buffer: Mutex::new((0, buffer)),
            input_channels: Mutex::new(input_channels),
            error_channel,
            output_balancer: Mutex::new(output_balancer),
            message_buffer_span: debug_span!(target: "services", parent: parent_span, "Message buffer"),
        }
    }
}

impl<T> MessageBuffer<T>
where
    T: Debug,
{
    #[instrument]
    pub async fn run_input(self: Arc<Self>) {
        'select_loop: loop {
            let input_round = info_span!(target: "buffer_input", parent: &self.message_buffer_span, "Input round");
            let wait_for_next_message =
                info_span!(target: "buffer_input", parent: &input_round, "Next message");
            let mut next_message = async {
                let mut input_channels = self.input_channels.lock().await;
                let (payload, source_id) =
                    match future::select_all(input_channels.iter_mut().map(|r| Box::pin(r.recv())))
                        .await
                    {
                        (Some(message), idx, _) => (message, idx),
                        (None, idx, _) => panic!("Input channel {idx} has suddenly closed"),
                    };
                info!(target: "lifecycle_events", message = ?payload, gen_id = source_id, "Received a new message");
                let tracing_span = info_span!(target: "lifecycle_events", "Message is buffered", message = ?payload, gen_id = source_id);
                MessageEntry {
                    registered: time::Instant::now(),
                    source_id,
                    payload,
                    tracing_span,
                }
            }
            .instrument(wait_for_next_message)
            .await;
            let search_for_buffer =
                info_span!(target: "buffer_input", parent: &input_round, "Search for buffer space");
            {
                let mut message_buffer_lock = self.message_buffer.lock().await;
                let _search_for_buffer = search_for_buffer.enter();
                let (last_idx, buf) = message_buffer_lock.deref_mut();
                let mut next_idx = ((*last_idx).clone() + 1) % buf.len();
                while next_idx != *last_idx {
                    let candidate = &mut buf[next_idx];
                    if candidate.is_none() {
                        info!(
                            target: "lifecycle_events",
                            message = ?next_message.payload,
                            gen_id = next_message.source_id,
                            buf_idx = next_idx,
                            "Put into buffer"
                        );
                        *candidate = Some(next_message);
                        *last_idx = next_idx;
                        continue 'select_loop;
                    }
                    next_idx = (next_idx + 1) % buf.len();
                }
            };
            let discard_old_message =
                info_span!(target: "buffer_input", parent: &input_round, "Discard old message");
            async {
                let mut message_buffer_lock = self.message_buffer.lock().await;
                let (last_idx, buf) = message_buffer_lock.deref_mut();
                if let Some(last_insert) = &mut buf[*last_idx] {
                    mem::swap(last_insert, &mut next_message);
                    info!(
                        target: "lifecycle_events",
                        message = ?next_message.payload,
                        gen_id = next_message.source_id,
                        buf_idx = *last_idx,
                        "Evicted from buffer"
                    );
                    if let Err(send_error) = self.error_channel.send(next_message).await {
                        panic!("Failed sending to error channel: {send_error}");
                    }
                } else {
                    unreachable!();
                }
            }
            .instrument(discard_old_message)
            .await
        }
    }

    #[instrument]
    pub async fn run_output(self: Arc<Self>) {
        let mut output_balancer = self.output_balancer.lock().await;
        loop {
            let output_round = info_span!(target: "buffer_output", parent: &self.message_buffer_span, "Output round");
            let wait_for_next_sink =
                info_span!(target: "buffer_output", parent: &output_round, "Wait for next sink");
            let next_slot = output_balancer
                .next_ready()
                .instrument(wait_for_next_sink)
                .await;
            let search_for_next_message = info_span!(target: "buffer_output", parent: &output_round, "Search for next message");
            let mut message_buffer_lock = self.message_buffer.lock().await;
            search_for_next_message.in_scope(move || {
                let (_, buf) = message_buffer_lock.deref_mut();
                let mut ref_vec: Vec<_> = buf.iter_mut().filter(|o| o.is_some()).collect();
                ref_vec.sort_unstable_by(|a, b| match (a, b) {
                    (Some(a), Some(b)) => match a.source_id.cmp(&b.source_id) {
                        std::cmp::Ordering::Equal => a.registered.cmp(&b.registered),
                        ord => ord,
                    },
                    _ => unreachable!(),
                });
                if let Some(next_val) = ref_vec.into_iter().next() {
                    let mut send_val = None;
                    mem::swap(next_val, &mut send_val);
                    if let Some(send_val) = send_val {
                        info!(
                            target: "lifecycle_messages",
                            message = ?send_val.payload,
                            gen_id = send_val.source_id,
                            "Pushing next message"
                        );
                        next_slot.send(send_val.payload);
                    } else {
                        unreachable!();
                    }
                }
            })
        }
    }
}
